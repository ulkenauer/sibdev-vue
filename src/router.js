import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: 'login'
    },
    {
      path: '/login',
      name: 'login',
      beforeEnter(to, from, next) {
        if (localStorage.getItem('token') === null) {
          next()
        } else {
          next({ name: 'Dashboard' })
        }
      },
      component: () => import(/* webpackChunkName: "about" */ './views/Login.vue')
    },
    {
      path: '/user',
      component: () => import(/* webpackChunkName: "about" */ './components/BaseUser.vue'),
      beforeEnter(to, from, next) {
        if (localStorage.getItem('token') === null) {
          next({ name: 'login' })
        } else {
          next()
        }
      },
      children: [
        { path: '', redirect: 'dashboard' },
        { name: 'Dashboard', path: 'dashboard', component: () => import(/* webpackChunkName: "about" */ './views/Dashboard.vue') },
        { name: 'Management', path: 'management', component: () => import(/* webpackChunkName: "about" */ './views/Management.vue') },
        { name: 'Auxiliary', path: 'auxiliary', component: () => import(/* webpackChunkName: "about" */ './views/Auxiliary.vue') },
      ]
    },
    {
      path: '/404',
      name: 'NotFound',
      component: () => import(/* webpackChunkName: "about" */ './views/NotFound.vue')
    },
    {
      path: '*',
      redirect: { name: 'NotFound' }
    },
  ]
})
