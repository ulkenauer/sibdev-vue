import First from './First'
import Second from './Second'
import Penguin from './Penguin'
export default {
    First,
    Second,
    Penguin
}