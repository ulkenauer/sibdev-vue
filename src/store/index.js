import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

import ApiCards from '@/api-methods.json'

export default new Vuex.Store({
  state: {
    users: [],
    currentUser: null,
    count: 0,
    chosenCard: null,
    params: {},
    cards: []
  },
  getters: {
    usersCount: state => {
      return state.users.length
    },
    userInfo: state => {
      if(state.currentUser === null) {
        return null
      } else {
        return state.users[state.currentUser]
      }
    }
  },
  mutations: {
    selectQueryCard(state, data) {
      state.chosenCard = data.idx
      state.params = data.params
    },
    randomizeCards(state) {
      let cards = []
      ApiCards.forEach((element, idx) => {
        cards.push(idx)
      })
      
      let limit = cards.length

      let result = []

      for (let index = 0; index < limit; index++) {
        let cardIdx = Math.floor(Math.random() * cards.length)
        result.push(cards[cardIdx])
        cards.splice(cardIdx, 1)
      }

      state.cards = result

      console.log(`limit is ${limit}`)
    },
    addUser(state, user) {
      let isString = function (value) {
        return typeof value === 'string' || value instanceof String;
      }
      try {
        if(!isString(user.first_name) || user.first_name.length === 0) {
          throw new Error('user object is invalid')
        }
        if(!isString(user.last_name)  || user.last_name.length === 0) {
          throw new Error('user object is invalid')
        }
        if(!isString(user.login)  || user.login.length === 0) {
          throw new Error('user object is invalid')
        }
      } catch (e) {
        throw new Error('user object is invalid')
      }
      state.users.push(user)
    },
    selectUser(state, idx)
    {
      try {
        if(Number.isInteger(idx) === false) {
          throw new Error('idx is invalid')
        }
        if(idx >= state.users.length || idx < 0) {
          throw new Error('idx is invalid')
        }
      } catch(e) {
        throw new Error('idx is invalid')
      }
      state.currentUser = idx
    },
    increment: state => state.count++,
    decrement: state => state.count--
  }
})